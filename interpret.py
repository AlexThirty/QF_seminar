import tensorflow as tf
import numpy as np
import QuantLib as ql
import matplotlib
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from deepexplain.tensorflow import DeepExplain
print(tf.__version__)


v0_low = 0.0001
v0_up = 0.04
rho_low = -0.95
rho_up = -0.1
sigma_low = 0.01
sigma_up = 1.0
theta_low = 0.01
theta_up = 0.2
kappa_low = 1.0
kappa_up = 10.0
v0_mean = (v0_low + v0_up)/2
rho_mean = (rho_low + rho_up)/2
sigma_mean = (sigma_low + sigma_up)/2
theta_mean = (theta_low + theta_up)/2
kappa_mean = (kappa_low + kappa_up)/2

S0 = 1
N = 12000
norm_perc_add = 0.25
T = [0.1, 0.3, 0.6, 0.9, 1.2, 1.5, 1.8, 2.0]
K = [0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5]

labels = np.array([])
for t in T:
    for k in K:
        labels = np.append(labels, ["T="+str(t)+", K="+str(k)])

print(labels)

def heatmap(data, row_labels, col_labels, method_name, ax=None,
            cbar_kw={}, cbarlabel="", **kwargs):

    if not ax:
        ax = plt.gca()

    # Plot the heatmap
    im = ax.imshow(data, **kwargs)
    ax.set_title(str(method_name) + ' heatmap')
    ax.set_ylabel('T')
    ax.set_xlabel('K')
    # Create colorbar
    cbar = ax.figure.colorbar(im, ax=ax, **cbar_kw)

    # We want to show all ticks...
    ax.set_xticks(np.arange(data.shape[1]))
    ax.set_yticks(np.arange(data.shape[0]))
    # ... and label them with the respective list entries.
    ax.set_xticklabels(col_labels)
    ax.set_yticklabels(row_labels)

    # Let the horizontal axes labeling appear on top.
    ax.tick_params(top=False, bottom=True,
                   labeltop=False, labelbottom=True)

    # Rotate the tick labels and set their alignment.

    # Turn spines off and create white grid.
    #ax.spines[:].set_visible(False)

    ax.set_xticks(np.arange(data.shape[1]+1)-.5, minor=True)
    ax.set_yticks(np.arange(data.shape[0]+1)-.5, minor=True)
    ax.grid(which="minor", color="w", linestyle='-', linewidth=3)
    ax.tick_params(which="minor", bottom=False, left=False)

    return im, cbar


def annotate_heatmap(im, data=None, valfmt="{x:.2f}",
                     textcolors=('white', 'black'),
                     threshold=None, **textkw):

    if not isinstance(data, (list, np.ndarray)):
        data = im.get_array()

    # Normalize the threshold to the images color range.
    if threshold is not None:
        threshold = im.norm(threshold)
    else:
        threshold = im.norm(data.max())/2.

    # Set default alignment to center, but allow it to be
    # overwritten by textkw.
    kw = dict(horizontalalignment="center",
              verticalalignment="center")
    kw.update(textkw)

    # Get the formatter in case a string is supplied
    if isinstance(valfmt, str):
        valfmt = matplotlib.ticker.StrMethodFormatter(valfmt)

    # Loop over the data and create a `Text` for each "pixel".
    # Change the text's color depending on the data.
    texts = []
    for i in range(data.shape[0]):
        for j in range(data.shape[1]):
            kw.update(color=textcolors[int(im.norm(data[i, j]) > threshold)])
            text = im.axes.text(j, i, valfmt(data[i, j], None), **kw)
            texts.append(text)

    return texts

def plot_heatmap(data, method_name):
    fig, ax = plt.subplots()

    im, cbar = heatmap(data, T, K, ax=ax, cmap="inferno", method_name=method_name)
    texts = annotate_heatmap(im, valfmt="{x:.2f}")

    fig.tight_layout()
    plt.savefig(method_name+'_heatmap.png')

def plot_bar(data, method_name):
    data_flatten = data.reshape(-1)
    labels_sorted = [labels[i] for i in np.argsort(data_flatten)]
    data_sorted = np.sort(data_flatten)
    fig = plt.figure()

    plt.barh(labels_sorted[-20:], data_sorted[-20:])
    plt.title(method_name+' barplot')
    plt.tight_layout()
    plt.savefig(method_name+'_barplot.png')


def plot_stackbar(data_v0, data_rho, data_sigma, data_theta, data_kappa, method_name):
    data_total = data_v0 + data_rho + data_sigma + data_theta + data_kappa
    data_flatten = data_total.reshape(-1)
    data_v0_flatten = data_v0.reshape(-1)
    data_rho_flatten = data_rho.reshape(-1)
    data_sigma_flatten = data_sigma.reshape(-1)
    data_theta_flatten = data_theta.reshape(-1)
    data_kappa_flatten = data_kappa.reshape(-1)
    labels_sorted = [labels[i] for i in np.argsort(data_flatten)]
    data_v0_sorted = np.array([data_v0_flatten[i] for i in np.argsort(data_flatten)])
    data_rho_sorted = np.array([data_rho_flatten[i] for i in np.argsort(data_flatten)])
    data_sigma_sorted = np.array([data_sigma_flatten[i] for i in np.argsort(data_flatten)])
    data_theta_sorted = np.array([data_theta_flatten[i] for i in np.argsort(data_flatten)])
    data_kappa_sorted = np.array([data_kappa_flatten[i] for i in np.argsort(data_flatten)])
    fig, ax = plt.subplots()
    ax.barh(labels_sorted[-20:], data_v0_sorted[-20:], label='v0')
    ax.barh(labels_sorted[-20:], data_rho_sorted[-20:], left=data_v0_sorted[-20:], label='rho')
    ax.barh(labels_sorted[-20:], data_sigma_sorted[-20:], left=data_v0_sorted[-20:]+data_rho_sorted[-20:], label='sigma')
    ax.barh(labels_sorted[-20:], data_theta_sorted[-20:], left=data_v0_sorted[-20:]+data_rho_sorted[-20:]+data_sigma_sorted[-20:], label='theta')
    ax.barh(labels_sorted[-20:], data_kappa_sorted[-20:], left=data_v0_sorted[-20:]+data_rho_sorted[-20:]+data_sigma_sorted[-20:]+data_theta_sorted[-20:], label='kappa')

    ax.set_title('SHAP values')
    ax.legend()
    plt.tight_layout()
    plt.savefig(method_name+'_stackplot.png')

    plot_heatmap(data_total, method_name)

def error_analisys(prediction, actual, method_name, phase, target):
    nbins = 100
    errors = prediction - actual
    rel_errors = abs(errors)/abs(actual)

    fig, axs = plt.subplots(1, 3, sharey=False, figsize=[12,8])

    axs[0].plot(actual, prediction, '.')
    axs[0].set_xlabel("Actual")
    axs[0].set_ylabel("Prediction")
    axs[0].set_title(method_name + " " + phase + " " + target)

    axs[1].plot(range(len(actual)), rel_errors, '.')
    axs[1].set_ylabel("Relative error")
    axs[1].set_title(method_name + " " + phase + " " + target + " relative error: "+str(round(np.mean(rel_errors),3)))

    axs[2].hist(errors, bins=nbins)
    axs[2].set_ylabel("frequency")
    axs[2].set_xlabel("error")
    axs[2].set_title(method_name + " " + phase + " " + target +  " errors")
    plt.tight_layout()
    plt.savefig(method_name+'_'+phase+'_'+target+'_errors.png')



def v0_act(x):
    return tf.keras.activations.tanh(x)*(v0_up*(1.+norm_perc_add)-v0_low*(1.+norm_perc_add))/2 + (v0_up*(1.+norm_perc_add)+v0_low*(1.+norm_perc_add))/2

def rho_act(x):
    return tf.keras.activations.tanh(x)*(rho_up*(1.+norm_perc_add)-rho_low*(1.+norm_perc_add))/2 + (rho_up*(1.+norm_perc_add)+rho_low*(1.+norm_perc_add))/2

def sigma_act(x):
    return tf.keras.activations.tanh(x)*(sigma_up*(1.+norm_perc_add)-sigma_low*(1.+norm_perc_add))/2 + (sigma_up*(1.+norm_perc_add)+sigma_low*(1.+norm_perc_add))/2

def theta_act(x):
    return tf.keras.activations.tanh(x)*(theta_up*(1.+norm_perc_add)-theta_low*(1.+norm_perc_add))/2 + (theta_up*(1.+norm_perc_add)+theta_low*(1.+norm_perc_add))/2

def kappa_act(x):
    return tf.keras.activations.tanh(x)*(kappa_up*(1.+norm_perc_add)-kappa_low*(1.+norm_perc_add))/2 + (kappa_up*(1.+norm_perc_add)+kappa_low*(1.+norm_perc_add))/2


def generate_params(tot_dim, train_prop):
    v0_rand = np.random.uniform(v0_low, v0_up, tot_dim)
    rho_rand = np.random.uniform(rho_low, rho_up, tot_dim)
    sigma_rand = np.random.uniform(sigma_low, sigma_up, tot_dim)
    theta_rand = np.random.uniform(theta_low, theta_up, tot_dim)
    kappa_rand = np.random.uniform(kappa_low, kappa_up, tot_dim)
    params = np.stack((v0_rand.T, rho_rand.T, sigma_rand.T,
        theta_rand.T, kappa_rand.T), axis=-1)

    valid = params[2*params[:,4]*params[:,3] > params[:,2]**2]
    np.random.shuffle(valid)
    train_dim = np.floor(len(valid)*train_prop).astype(int)
    test_dim = len(valid) - train_dim
    slc_train = (slice(0, train_dim), slice(None))
    slc_test = (slice(train_dim, len(valid)), slice(None))
    train_params, test_params = valid[slc_train], valid[slc_test]
    return (train_params, test_params)

def generate_data(params):
    # World State setup
    spot = S0
    rate = 0.0
    today = ql.Date(1, 7, 2020)

    # Set up the flat risk-free curves
    riskFreeCurve = ql.FlatForward(today, rate, ql.Actual365Fixed())
    flat_ts = ql.YieldTermStructureHandle(riskFreeCurve)
    dividend_ts = ql.YieldTermStructureHandle(riskFreeCurve)

    # Setting up a Heston model with dummy parameters (roughly 10% constant BS vol)
    v0 = params[0]
    kappa = params[4]
    theta = params[3]
    rho = params[1]
    sigma = params[2]

    process = ql.HestonProcess(flat_ts, dividend_ts,
        ql.QuoteHandle(ql.SimpleQuote(spot)), v0, kappa, theta, sigma, rho)

    # Boilerplate to get to the Vol Surface object
    heston_model = ql.HestonModel(process)
    heston_handle = ql.HestonModelHandle(heston_model)
    heston_vol_surface = ql.HestonBlackVolSurface(heston_handle)

    vol_mat = np.zeros((len(T), len(K)))
    for i in range(len(T)):
        for j in range(len(K)):
            vol_mat[i][j] = heston_vol_surface.blackVol(T[i], K[j])
    return vol_mat



(train_params, test_params) = generate_params(N, 0.85)
n_train = len(train_params)
n_test = len(test_params)

train_data = np.zeros(shape = (n_train, len(T), len(K)))
print("Generating training data")
for i in range(n_train):
    if np.mod(i,1000) == 0:
        print(i)
    train_data[i] = generate_data(train_params[i])
print("Training data generation complete")

test_data = np.zeros(shape = (n_test, len(T), len(K)))
print("Generating test data")
for i in range(n_test):
    if np.mod(i,1000) == 0:
        print(i)
    test_data[i] = generate_data(test_params[i])
print("Test data generation complete")

# FCNN

# Data normalization
FCNN_train_data = train_data / np.max(np.concatenate([train_data, test_data]))
FCNN_test_data = test_data / np.max(np.concatenate([train_data, test_data]))

FCNN_train_params = np.zeros(shape=(len(train_params),5))
FCNN_train_params[:,0] = (train_params[:,0] - v0_low)/(v0_up - v0_low)
FCNN_train_params[:,1] = (train_params[:,1] - rho_low)/(rho_up - rho_low)
FCNN_train_params[:,2] = (train_params[:,2] - sigma_low)/(sigma_up - sigma_low)
FCNN_train_params[:,3] = (train_params[:,3] - theta_low)/(theta_up - theta_low)
FCNN_train_params[:,4] = (train_params[:,4] - kappa_low)/(kappa_up - kappa_low)

FCNN_test_params = np.zeros(shape=(len(test_params),5))
FCNN_test_params[:,0] = (test_params[:,0] - v0_low)/(v0_up - v0_low)
FCNN_test_params[:,1] = (test_params[:,1] - rho_low)/(rho_up - rho_low)
FCNN_test_params[:,2] = (test_params[:,2] - sigma_low)/(sigma_up - sigma_low)
FCNN_test_params[:,3] = (test_params[:,3] - theta_low)/(theta_up - theta_low)
FCNN_test_params[:,4] = (test_params[:,4] - kappa_low)/(kappa_up - kappa_low)

tf.keras.backend.clear_session()

FCNN_session = tf.Session()
tf.compat.v1.keras.backend.set_session(FCNN_session)

FCNN_data_input = tf.keras.Input(
    (8,11)
)
FCNN_flatten = tf.keras.layers.Flatten()(FCNN_data_input)
FCNN_first = tf.keras.layers.Dense(67, activation='elu')(FCNN_flatten)
FCNN_second = tf.keras.layers.Dense(46, activation='elu')(FCNN_first)
FCNN_third = tf.keras.layers.Dense(25, activation='elu')(FCNN_second)
FCNN_output = tf.keras.layers.Dense(5, activation='hard_sigmoid')(FCNN_third)


FCNN_model = tf.keras.Model(
    inputs = [FCNN_data_input],
    outputs = [FCNN_output]
)

print('FCNN model summary:')
print(FCNN_model.summary())

tf.keras.utils.plot_model(FCNN_model, "FCNN_model.png", show_shapes=True)
FCNN_model.compile(
    optimizer='adam',
    loss=tf.keras.losses.MeanSquaredLogarithmicError(),
    metrics=['msle']
)

FCNN_model.fit(FCNN_train_data, FCNN_train_params, epochs=1000)
FCNN_test_loss = FCNN_model.evaluate(FCNN_test_data,  FCNN_test_params, verbose=2)
FCNN_train_pred = FCNN_model.predict(FCNN_train_data)
FCNN_test_pred = FCNN_model.predict(FCNN_test_data)

FCNN_train_pred[:,0] = v0_low + FCNN_train_pred[:,0]*(v0_up - v0_low)
FCNN_train_pred[:,1] = rho_low + FCNN_train_pred[:,1]*(rho_up - rho_low)
FCNN_train_pred[:,2] = sigma_low + FCNN_train_pred[:,2]*(sigma_up - sigma_low)
FCNN_train_pred[:,3] = theta_low + FCNN_train_pred[:,3]*(theta_up - theta_low)
FCNN_train_pred[:,4] = kappa_low + FCNN_train_pred[:,4]*(kappa_up - kappa_low)

FCNN_test_pred[:,0] = v0_low + FCNN_test_pred[:,0]*(v0_up - v0_low)
FCNN_test_pred[:,1] = rho_low + FCNN_test_pred[:,1]*(rho_up - rho_low)
FCNN_test_pred[:,2] = sigma_low + FCNN_test_pred[:,2]*(sigma_up - sigma_low)
FCNN_test_pred[:,3] = theta_low + FCNN_test_pred[:,3]*(theta_up - theta_low)
FCNN_test_pred[:,4] = kappa_low + FCNN_test_pred[:,4]*(kappa_up - kappa_low)

params = ['v0', 'rho', 'sigma', 'theta', 'kappa']

for i in range(5):
    error_analisys(FCNN_train_pred[:,i], train_params[:,i], "FCNN", "train", params[i])
    error_analisys(FCNN_test_pred[:,i], test_params[:,i], "FCNN", "test", params[i])

xs = FCNN_test_data
baseline = np.zeros(shape=[1,8,11])
with DeepExplain(session=FCNN_session) as de:
    X = tf.compat.v1.placeholder(tf.float32, shape=(None, 8, 11))
    FCNN_attributions = {
        'FCNN Epsilon-LRP':         de.explain('elrp', FCNN_model(X), X, xs),
        'FCNN SHAP v0':             de.explain('shapley_sampling', FCNN_model(X), X, xs, ys=[[1,0,0,0,0]]),
        'FCNN SHAP rho':            de.explain('shapley_sampling', FCNN_model(X), X, xs, ys=[[0,1,0,0,0]]),
        'FCNN SHAP sigma':          de.explain('shapley_sampling', FCNN_model(X), X, xs, ys=[[0,0,1,0,0]]),
        'FCNN SHAP theta':          de.explain('shapley_sampling', FCNN_model(X), X, xs, ys=[[0,0,0,1,0]]),
        'FCNN SHAP kappa':          de.explain('shapley_sampling', FCNN_model(X), X, xs, ys=[[0,0,0,0,1]]),
        #'FCNN DeepLIFT':            de.explain('deeplift', FCNN_model(X), X, xs, baseline=np.zeros((8,11))),
    }


for i, method_name in enumerate(FCNN_attributions):
    plot_heatmap(np.mean(abs(FCNN_attributions[method_name]), axis=0), method_name)
    plot_bar(np.mean(abs(FCNN_attributions[method_name]), axis=0), method_name)

plot_stackbar(
    np.mean(abs(FCNN_attributions['FCNN SHAP v0']), axis=0),
    np.mean(abs(FCNN_attributions['FCNN SHAP rho']), axis=0),
    np.mean(abs(FCNN_attributions['FCNN SHAP sigma']), axis=0),
    np.mean(abs(FCNN_attributions['FCNN SHAP theta']), axis=0),
    np.mean(abs(FCNN_attributions['FCNN SHAP kappa']), axis=0),
    'FCNN Shapley'
)

# CNN

CNN_scaler = StandardScaler(with_mean=False, with_std=False)
train_data_reshaped = train_data.reshape(
    (train_data.shape[0], train_data.shape[1]*train_data.shape[2])
)
CNN_scaler.fit(train_data_reshaped)
CNN_train_data = CNN_scaler.transform(train_data_reshaped).reshape(
    (train_data.shape[0], train_data.shape[1], train_data.shape[2], 1)
)
CNN_train_params = train_params

test_data_reshaped = test_data.reshape(
    (test_data.shape[0], test_data.shape[1]*test_data.shape[2])
)
CNN_test_data = CNN_scaler.transform(test_data_reshaped).reshape(
    (test_data.shape[0], test_data.shape[1], test_data.shape[2], 1)
)
CNN_test_params = test_params


tf.keras.backend.clear_session()

CNN_session = tf.Session()
tf.compat.v1.keras.backend.set_session(CNN_session)

CNN_data_input = tf.keras.Input(
    (8,11,1), name = 'data'
)
CNN_conv2D = tf.keras.layers.Conv2D(32, (3,3), activation='elu')(CNN_data_input)
CNN_maxPooling2D = tf.keras.layers.MaxPool2D((2,2))(CNN_conv2D)
CNN_flatten = tf.keras.layers.Flatten()(CNN_maxPooling2D)
CNN_dense = tf.keras.layers.Dense(50,activation='elu')(CNN_flatten)
CNN_v0_pred = tf.keras.layers.Dense(1,activation=v0_act)(CNN_dense)
CNN_rho_pred = tf.keras.layers.Dense(1,activation=rho_act)(CNN_dense)
CNN_sigma_pred = tf.keras.layers.Dense(1,activation=sigma_act)(CNN_dense)
CNN_theta_pred = tf.keras.layers.Dense(1,activation=theta_act)(CNN_dense)
CNN_kappa_pred = tf.keras.layers.Dense(1,activation=kappa_act)(CNN_dense)
CNN_output = tf.keras.layers.concatenate([
    CNN_v0_pred, CNN_rho_pred, CNN_sigma_pred, CNN_theta_pred, CNN_kappa_pred
])

CNN_model = tf.keras.Model(
    inputs = [CNN_data_input],
    outputs = [CNN_output]
)

print('CNN model summary:')
print(CNN_model.summary())

tf.keras.utils.plot_model(CNN_model, "CNN_model.png", show_shapes=True)
CNN_model.compile(
    optimizer='adam',
    loss='mse'
)

CNN_model.fit(CNN_train_data, CNN_train_params, epochs=2000)
CNN_test_loss = CNN_model.evaluate(CNN_test_data,  CNN_test_params, verbose=2)
CNN_test_pred = CNN_model.predict(CNN_test_data)
CNN_train_pred = CNN_model.predict(CNN_train_data)

for i in range(5):
    error_analisys(CNN_train_pred[:,i], train_params[:,i], "CNN", "train", params[i])
    error_analisys(CNN_test_pred[:,i], test_params[:,i], "CNN", "test", params[i])

xs = CNN_test_data

with DeepExplain(session=CNN_session) as de:
    X = tf.compat.v1.placeholder(tf.float32, shape=(None, 8, 11, 1))
    CNN_attributions = {
        'CNN Epsilon-LRP':      de.explain('elrp', CNN_model(X), X, xs),
        'CNN SHAP v0':               de.explain('shapley_sampling', CNN_model(X), X, xs, ys=[[1,0,0,0,0]]),
        'CNN SHAP rho':              de.explain('shapley_sampling', CNN_model(X), X, xs, ys=[[0,1,0,0,0]]),
        'CNN SHAP sigma':            de.explain('shapley_sampling', CNN_model(X), X, xs, ys=[[0,0,1,0,0]]),
        'CNN SHAP theta':            de.explain('shapley_sampling', CNN_model(X), X, xs, ys=[[0,0,0,1,0]]),
        'CNN SHAP kappa':            de.explain('shapley_sampling', CNN_model(X), X, xs, ys=[[0,0,0,0,1]]),
        #'DeepLIFT (Rescale)':    de.explain('deeplift', CNN_model(X), X, xs),
    }

for i, method_name in enumerate(CNN_attributions):
    plot_heatmap(np.mean(abs(CNN_attributions[method_name]), axis=0).reshape(8,11), method_name)
    plot_bar(np.mean(abs(CNN_attributions[method_name]), axis=0).reshape(8,11), method_name)


plot_stackbar(
    np.mean(abs(CNN_attributions['CNN SHAP v0']/v0_mean), axis=0).reshape(8,11),
    np.mean(abs(CNN_attributions['CNN SHAP rho']/rho_mean), axis=0).reshape(8,11),
    np.mean(abs(CNN_attributions['CNN SHAP sigma']/sigma_mean), axis=0).reshape(8,11),
    np.mean(abs(CNN_attributions['CNN SHAP theta']/theta_mean), axis=0).reshape(8,11),
    np.mean(abs(CNN_attributions['CNN SHAP kappa']/kappa_mean), axis=0).reshape(8,11),
    'CNN Shapley'
)
