import tensorflow as tf
import numpy as np
import QuantLib as ql
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler


v0_low = 0.0001
v0_up = 0.04
rho_low = -0.95
rho_up = -0.1
sigma_low = 0.01
sigma_up = 1.0
theta_low = 0.01
theta_up = 0.2
kappa_low = 1.0
kappa_up = 10.0

S0 = 1
N = 1000
n = 10000
T = [0.1, 0.3, 0.6, 0.9, 1.2, 1.5, 1.8, 2.0]
K = [0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5]

def v0_act(x):
    return tf.keras.activations.tanh(x)*(v0_up-v0_low)/2 + (v0_up+v0_low)/2

def rho_act(x):
    return tf.keras.activations.tanh(x)*(rho_up-rho_low)/2 + (rho_up+rho_low)/2

def sigma_act(x):
    return tf.keras.activations.tanh(x)*(sigma_up-sigma_low)/2 + (sigma_up+sigma_low)/2

def theta_act(x):
    return tf.keras.activations.tanh(x)*(theta_up-theta_low)/2 + (theta_up+theta_low)/2

def kappa_act(x):
    return tf.keras.activations.tanh(x)*(kappa_up-kappa_low)/2 + (kappa_up+kappa_low)/2


def generate_params(tot_dim, train_prop):
    v0_rand = np.random.uniform(v0_low, v0_up, tot_dim)
    rho_rand = np.random.uniform(rho_low, rho_up, tot_dim)
    sigma_rand = np.random.uniform(sigma_low, sigma_up, tot_dim)
    theta_rand = np.random.uniform(theta_low, theta_up, tot_dim)
    kappa_rand = np.random.uniform(kappa_low, kappa_up, tot_dim)
    params = np.stack((v0_rand.T, rho_rand.T, sigma_rand.T,
        theta_rand.T, kappa_rand.T), axis=-1)

    valid = params[2*params[:,4]*params[:,3] > params[:,2]**2]
    np.random.shuffle(valid)
    train_dim = np.floor(len(valid)*train_prop).astype(int)
    test_dim = len(valid) - train_dim
    slc_train = (slice(0, train_dim), slice(None))
    slc_test = (slice(train_dim, len(valid)), slice(None))
    train_params, test_params = valid[slc_train], valid[slc_test]
    return (train_params, test_params)

def generate_data(params):
    # World State setup
    spot = S0
    rate = 0.0
    today = ql.Date(1, 7, 2020)

    # Set up the flat risk-free curves
    riskFreeCurve = ql.FlatForward(today, rate, ql.Actual365Fixed())
    flat_ts = ql.YieldTermStructureHandle(riskFreeCurve)
    dividend_ts = ql.YieldTermStructureHandle(riskFreeCurve)

    # Setting up a Heston model with dummy parameters (roughly 10% constant BS vol)
    v0 = params[0]
    kappa = params[4]
    theta = params[3]
    rho = params[1]
    sigma = params[2]

    process = ql.HestonProcess(flat_ts, dividend_ts,
        ql.QuoteHandle(ql.SimpleQuote(spot)), v0, kappa, theta, sigma, rho)

    # Boilerplate to get to the Vol Surface object
    heston_model = ql.HestonModel(process)
    heston_handle = ql.HestonModelHandle(heston_model)
    heston_vol_surface = ql.HestonBlackVolSurface(heston_handle)

    vol_mat = np.zeros((len(T), len(K)))
    for i in range(len(T)):
        for j in range(len(K)):
            vol_mat[i][j] = heston_vol_surface.blackVol(T[i], K[j])
    return vol_mat



(train_params, test_params) = generate_params(10000, 0.85)
n_train = len(train_params)
n_test = len(test_params)

train_data = np.zeros(shape = (n_train, len(T), len(K)))
print("Generating training data")
for i in range(n_train):
    if np.mod(i,1000) == 0:
        print(i)
    train_data[i] = generate_data(train_params[i])
print("Training data generation complete")

test_data = np.zeros(shape = (n_test, len(T), len(K)))
print("Generating test data")
for i in range(n_test):
    if np.mod(i,1000) == 0:
        print(i)
    test_data[i] = generate_data(test_params[i])
print("Test data generation complete")

# Data normalization
FCNN_train_data = train_data / np.max(np.concatenate([train_data, test_data]))
FCNN_test_data = test_data / np.max(np.concatenate([train_data, test_data]))

FCNN_train_params = np.zeros(shape=(len(train_params),5))
FCNN_train_params[:,0] = (train_params[:,0] - v0_low)/(v0_up - v0_low)
FCNN_train_params[:,1] = (train_params[:,1] - rho_low)/(rho_up - rho_low)
FCNN_train_params[:,2] = (train_params[:,2] - sigma_low)/(sigma_up - sigma_low)
FCNN_train_params[:,3] = (train_params[:,3] - theta_low)/(theta_up - theta_low)
FCNN_train_params[:,4] = (train_params[:,4] - kappa_low)/(kappa_up - kappa_low)

FCNN_test_params = np.zeros(shape=(len(test_params),5))
FCNN_test_params[:,0] = (test_params[:,0] - v0_low)/(v0_up - v0_low)
FCNN_test_params[:,1] = (test_params[:,1] - rho_low)/(rho_up - rho_low)
FCNN_test_params[:,2] = (test_params[:,2] - sigma_low)/(sigma_up - sigma_low)
FCNN_test_params[:,3] = (test_params[:,3] - theta_low)/(theta_up - theta_low)
FCNN_test_params[:,4] = (test_params[:,4] - kappa_low)/(kappa_up - kappa_low)

FCNN_model = tf.keras.Sequential([
    tf.keras.layers.Flatten(input_shape=(8, 11)),
    tf.keras.layers.Dense(67, activation='elu'),
    tf.keras.layers.Dense(46, activation='elu'),
    tf.keras.layers.Dense(25, activation='elu'),
    tf.keras.layers.Dense(5, activation='hard_sigmoid')
])

print("FCNN model summary:")
print(FCNN_model.summary())

FCNN_model.compile(optimizer='adam',
    loss=tf.keras.losses.MeanSquaredLogarithmicError(),metrics=['msle'])

FCNN_model.fit(FCNN_train_data, FCNN_train_params, epochs=30)

FCNN_test_loss = FCNN_model.evaluate(FCNN_test_data,  FCNN_test_params, verbose=2)
FCNN_test_pred = FCNN_model.predict(FCNN_test_data)
FCNN_test_pred[:,0] = v0_low + FCNN_test_pred[:,0]*(v0_up - v0_low)
FCNN_test_pred[:,1] = rho_low + FCNN_test_pred[:,1]*(rho_up - rho_low)
FCNN_test_pred[:,2] = sigma_low + FCNN_test_pred[:,2]*(sigma_up - sigma_low)
FCNN_test_pred[:,3] = theta_low + FCNN_test_pred[:,3]*(theta_up - theta_low)
FCNN_test_pred[:,4] = kappa_low + FCNN_test_pred[:,4]*(kappa_up - kappa_low)

# Convolutional Neural Net

CNN_scaler = StandardScaler(with_mean=False, with_std=False)
train_data_reshaped = train_data.reshape(
    (train_data.shape[0], train_data.shape[1]*train_data.shape[2])
)
CNN_scaler.fit(train_data_reshaped)
CNN_train_data = CNN_scaler.transform(train_data_reshaped).reshape(
    (train_data.shape[0], train_data.shape[1], train_data.shape[2], 1)
)
CNN_train_params = train_params

test_data_reshaped = test_data.reshape(
    (test_data.shape[0], test_data.shape[1]*test_data.shape[2])
)
CNN_test_data = CNN_scaler.transform(test_data_reshaped).reshape(
    (test_data.shape[0], test_data.shape[1], test_data.shape[2], 1)
)
CNN_test_params = test_params

CNN_data_input = tf.keras.Input(
    (8,11,1), name = 'data'
)
CNN_conv2D = tf.keras.layers.Conv2D(32, (3,3), activation='elu')(CNN_data_input)
CNN_maxPooling2D = tf.keras.layers.MaxPool2D((2,2))(CNN_conv2D)
CNN_flatten = tf.keras.layers.Flatten()(CNN_maxPooling2D)
CNN_dense = tf.keras.layers.Dense(50,activation='elu')(CNN_flatten)
CNN_v0_pred = tf.keras.layers.Dense(1,activation=v0_act)(CNN_dense)
CNN_rho_pred = tf.keras.layers.Dense(1,activation=rho_act)(CNN_dense)
CNN_sigma_pred = tf.keras.layers.Dense(1,activation=sigma_act)(CNN_dense)
CNN_theta_pred = tf.keras.layers.Dense(1,activation=theta_act)(CNN_dense)
CNN_kappa_pred = tf.keras.layers.Dense(1,activation=kappa_act)(CNN_dense)
CNN_output = tf.keras.layers.concatenate([
    CNN_v0_pred, CNN_rho_pred, CNN_sigma_pred, CNN_theta_pred, CNN_kappa_pred
])

CNN_model = tf.keras.Model(
    inputs = [CNN_data_input],
    outputs = [CNN_output]
)

print('CNN model summary:')
print(CNN_model.summary())

tf.keras.utils.plot_model(CNN_model, "CNN_model.png", show_shapes=True)
CNN_model.compile(
    optimizer='adam',
    loss='mse'
)

CNN_model.fit(CNN_train_data, CNN_train_params, epochs=100)
CNN_test_loss = CNN_model.evaluate(CNN_test_data,  CNN_test_params, verbose=2)
CNN_test_pred = CNN_model.predict(CNN_test_data)
print(test_params[0])
print(CNN_test_pred[0])
